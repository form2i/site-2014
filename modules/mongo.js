class Collection {
    constructor(name) {
        this.name = name;

        this.docs = [];
    }
    
    findOne(query, callback) {
        const keys = Object.keys(query);
        const currentDoc = this.docs.find(doc =>
            keys.every(key => doc[key] === query[key])
        );

        console.log('findOne', this.name, query);
        callback(null, currentDoc);
    }

    find(query, callback) {
        const keys = Object.keys(query);
        const foundDocs = this.docs.filter(doc =>
            keys.every(key => doc[key] === query[key])
        );

        console.log('find', this.name, query);

        return {
            toArray: function(callback) {
                callback(null, foundDocs);
            }
        }
    }

    insert(doc, callback) {
        this.docs.push(doc);
        console.log('insert', this.name, doc);
        callback(null, this.docs);
    }

    update(query, update, callback) {
        const keys = Object.keys(query);
        this.docs = this.docs.map(doc => {
            if (keys.every(key => doc[key] === query[key])) {
                return Object.assign(doc, update);
            }
            return doc;
        });

        console.log('update', this.name, query, update);
        callback(null, this.docs);
    }

    remove(query, callback) {
        const keys = Object.keys(query);
        this.docs = this.docs.filter(doc =>
            !keys.every(key => doc[key] === query[key])
        );

        console.log('remove', this.name, query);
        callback(null, this.docs);
    }
}

const db = {};

db.users = new Collection('users');
db.projects = new Collection('projects');
db.lists = new Collection('lists');

module.exports = db;
