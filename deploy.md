## 简单部署备注

```sh
git clone https://gitlab.com/form2i/site-2014.git

cd site-2014

npm i

pm2 start app.js
```

增加 nginx 配置

```
server {
    listen       80;
    listen       [::]:80;
    server_name  site-2014.mirreal.net;
    root         /usr/share/nginx/html;

    include /etc/nginx/default.d/*.conf;

    error_page 404 /404.html;
    location = /404.html {
    }

    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
    }

    location / {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $http_host;
        proxy_pass http://127.0.0.1:7701;
    }
}
```

http://site-2014.mirreal.net/
